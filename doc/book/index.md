# Installation

## Using Composer

```bash
$ composer require laminas/laminas-eventmanager
```

## Learn

- [Quick start](quick-start.md)
- [Tutorial](tutorial.md)
- [Usage in a laminas-mvc application](application-integration/usage-in-a-laminas-mvc-application.md)
